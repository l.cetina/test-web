﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using TestApplication.Command;

namespace TestApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;
        private Subject<UpdateDataCommand> posted = new Subject<UpdateDataCommand>();

        public ValuesController(IMediator mediator, ILogger logger)
        {
            _mediator = mediator;
            _logger = logger;

            posted
                .GroupBy(x => x.StationId)
                .Select(x => x.Sample(TimeSpan.FromSeconds(10)))
                .Merge()
                .Subscribe(command =>
                {
                    _logger.Debug("Timer for stationId: {@station} has elapsed!", command.StationId);
                });
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost("Post")]
        public async Task Post([FromBody] UpdateDataCommand command)
        {
            _logger.Debug("{@controller}.Post called with argument: {@command}", nameof(ValuesController), command);
            await _mediator.Send(command);

            posted.OnNext(command);
        }
    }
}
