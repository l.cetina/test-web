﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Serilog;

namespace TestApplication.Command
{
    public class UpdateDataCommandHandler : IRequestHandler<UpdateDataCommand, Boolean>
    {
        private readonly ILogger _logger;

        public UpdateDataCommandHandler(ILogger logger)
        {
            _logger = logger;
           
        }

        public async Task<Boolean> Handle(UpdateDataCommand request, CancellationToken cancellationToken)
        {
            _logger.Debug("{@handler}.Handle called with param: {@command}", nameof(UpdateDataCommandHandler), request);
            return true;
        }
    }
}
