﻿using MediatR;

namespace TestApplication.Command
{
    public class UpdateDataCommand : IRequest<bool>
    {
        public int StationId { get; set; }
        public int Signal { get; set; }
        public int Var1 { get; set; }
    }
}